# Tempmail - you want to spam me? Sure, why not.

## What on earth is this?

This is an application written in PHP that allows to host a tempmail service. Every mail is deleted 10 minutes after it has been received.

---

## ToDo

1. HTML Template
2. AJAX-Integration (automatically refresh mails)
3. Assign Mails to Inboxes
4. Delete Mails after 10'
5. Compose Mails
6. Forward mails to your normal adress
7. Blacklist some Adresses
8. Nuke Adress (Delete everything (for the specified adress) without timer)